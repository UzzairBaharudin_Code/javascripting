// Accessing object Properties
// we can use dot notation or square brackets
// food.types - dot notation
// food['types] - square brackets
var food = {
    types: 'only pizza',
};

console.log(food.types);